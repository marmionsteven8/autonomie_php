<?php
    // mise en place des variables
    /* variable String */
    $prenom = "Steven";
    echo "Bonjour $prenom";
    /* variable int */
    $nombre = 1;
    /* variable decimal */
    $nombreAVirgule = 1.5;
    /* variable boolean */
    $testTrueOuFalse = true;
    /* renvoie le type de la variable et son contenu */
    var_dump($nombre);
    /* test d'égalité */
    echo $nombre==$nombreAVirgule;

    // manipulation des chaine de caracteres 
    $chaine = "bonjour tout le monde";
    /* afficher un caractere particulier de la chaine */ 
    echo $chaine[5];
    /* modifier un caractere de la chaine */
    $chaine[3]="0";
    /* Extraire une partie de la chaine */
    echo substr($chaine, 0, 4);
    echo substr($chaine, -1); // renvoie le caractère en partant de la fin 
    /* remplacer une partie de la chaine de caractere et fait attention aux majuscule ou non */
    str_replace("bonjour", 'au revoir', $chaine); 
    /* remplace une chaine de caractere en ne faisant pas at²tention aux majuscules ou non */
    str_ireplace('Bonjour', 'au revoir', $chaine);
    /* verifie si la chaine contient du texte mis en parametre */
    str_contains($chaine, 'bonjour');
    str_starts_with($chaine, 'bon'); // regarde si la chaine commence par "bon" == renvoie true //
    str_ends_with($chaine, 'onde'); // regarde si la chaine se termine par //
    $chaine2 = "test d'nelevement des espaces ";
    trim($chaine); // enleve l'espace en fin de chaine 

    // manipulation des nombres 
    /* Opération de base : addition possible, multiplication, division, soustraction --> */
    /* $res = $var1 + $var2, $res = $var1 * $var2, etc ... */
    $nb1 = 15;
    $nb2 = 36;
    /* faire un modulo */
    $res = $nb1 % $nb2;
    /* incrémenter */
    $nb1+=1;
    $nb1++; // incrémente de 1 aussi 
    /* décrémenter */
    $nb1-=1;
    $nb1--;

    // utilisation des tableaux
    /* declarer un tableau */
    $tableau = [];
    /* declarer ne mettant des valeurs */
    $tableau = ["bonjour", 2, 5.0, true, "au revoir"];
    /* avoir un element du tableau */
    echo $tableau[0];
    /* ajouter une valeur à la fin */
    $tableau[] = "au revoir";
    /* ajouter des valeurs à la fin */
    array_push($tableau, "ajout d'une valeur", 89, 5.0);
    /* ajouter une valeur au début */
    array_unshift($tableau, "val_debut");
    /* supprimer la valeur de fin du tableau */
    array_pop($tableau);
    /* récupérer la vleur supprimer */
    $valFin = array_pop($tableau);
    /* supprimer au debut */
    array_shift($tableau);
    /* récupérer cette valeur */
    $valDebut = array_shift($tableau);
?>